package com.sserra.data

import com.sserra.data.repos.ReviewsRepositoryImpl
import com.sserra.domain.Reviews
import com.sserra.domain.ReviewsCriteria
import com.sserra.domain.ReviewsRepository
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertEquals
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.io.IOException

/**
 * Unit tests for ReviewsRepositoryImpl
 */
class ReviewsRepositoryTests {

    // Object under test
    private lateinit var reviewsRepo: ReviewsRepository
    private lateinit var webServer: MockWebServer

    @Before
    fun setup() {
        // Start a mock web server
        webServer = MockWebServer()
        webServer.start()
        reviewsRepo = ReviewsRepositoryImpl(Api.create(webServer.url("")), memorySource = null)
    }

    @Test
    fun shouldCallReviewsEndpoint() {

        // Setup
        webServer.enqueue(
                MockResponse()
                        .setResponseCode(200)
                        .setBody(buildResponseBody())
        )

        val testObserver = TestObserver<Reviews>()
        reviewsRepo.getReviews(ReviewsCriteria(
                "berlin-l17",
                "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776")
        ).subscribe(testObserver)

        testObserver
                .await()
                .assertNoErrors()
                .assertValue {
                    it.size == 1
                }

        // Assert correct url and query params were sent
        val request = webServer.takeRequest()
        assertEquals("/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json", request.path.split("?").first())
        assertEquals("0", request.requestUrl.queryParameter("page"))
        assertEquals("10", request.requestUrl.queryParameter("count"))
        assertEquals("0", request.requestUrl.queryParameter("rating"))
        assertEquals("date_of_review", request.requestUrl.queryParameter("sortBy"))
        assertEquals("desc", request.requestUrl.queryParameter("direction"))

    }

    private fun buildResponseBody(): String {
        return "{\n" +
                "    \"status\": true,\n" +
                "    \"total_reviews_comments\": 582,\n" +
                "    \"data\": [\n" +
                "        {\n" +
                "            \"review_id\": 3979110,\n" +
                "            \"rating\": \"5.0\",\n" +
                "            \"title\": \"\",\n" +
                "            \"message\": \"This is a must see tour. We had to go on the German one as the days we were in Berlin the English tour was unavailable. It lasts over 2 hours and it truly is an amazing building/complex that is so hard to comprehend the magnitude of it. The airport is steeped in history concentrating on the 2nd World War &amp;;Cold War. You get to see so much of the building including going on the roof and the amazing entrance/terminal building of which the architecture is simply stunning. Make the time yo do this.\",\n" +
                "            \"author\": \"Peter – United Kingdom\",\n" +
                "            \"foreignLanguage\": false,\n" +
                "            \"date\": \"October 11, 2018\",\n" +
                "            \"date_unformatted\": {},\n" +
                "            \"languageCode\": \"en\",\n" +
                "            \"traveler_type\": \"family_young\",\n" +
                "            \"reviewerName\": \"Peter\",\n" +
                "            \"reviewerCountry\": \"United Kingdom\"\n" +
                "        }" +
                "   ]" +
                "}"
    }

    @After
    fun shutdown() {
        try {
            webServer.shutdown()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

}