package com.sserra.data

import com.sserra.data.entities.Response
import com.sserra.data.entities.Reviews
import io.reactivex.Single
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


/**
 * Api GetYourGuide
 * sergioserra99@gmail.com
 */
class Api {

    companion object {
        private const val ENDPOINT = "https://www.getyourguide.com/"
        private const val USER_AGENT = "GetYourGuide"

        val service: GetYourGuideService by lazy {
            create(HttpUrl.parse(ENDPOINT)!!)
        }

        fun create(endpoint: HttpUrl): GetYourGuideService {
            val httpClient = OkHttpClient.Builder()

            // Add user-agent header to every request
            httpClient.addInterceptor { chain ->
                val request = chain.request().newBuilder().addHeader("User-Agent", USER_AGENT).build()
                chain.proceed(request)
            }

            if (BuildConfig.DEBUG) {
                httpClient.addInterceptor(HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BASIC
                })
            }

            val retrofit = Retrofit.Builder()
                    .baseUrl(endpoint)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build())
                    .build()

            // Create an instance of our GetYourGuide API interface.
            return retrofit.create(GetYourGuideService::class.java)
        }

    }

    interface GetYourGuideService {

        @GET("{city}/{tour}/reviews.json")
        fun getReviews(
                @Path(value = "city", encoded = true) city: String,
                @Path(value = "tour", encoded = true) tour: String,
                @Query("page") page: Int = 0,
                @Query("count") count: Int = 10,
                @Query("rating") rating: Int = 0,
                @Query("type") type: String = "",
                @Query("sortBy") sort: String = "data_of_review",
                @Query("direction") direction: String = "DESC"
        ): Single<Response<Reviews>>

    }

}