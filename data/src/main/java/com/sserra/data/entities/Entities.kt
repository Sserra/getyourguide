package com.sserra.data.entities

import com.google.gson.annotations.SerializedName


data class Response<T>(
        @SerializedName("status")
        val status: Boolean,
        @SerializedName("total_reviews_comments")
        val totalReviewsComments: Int,
        @SerializedName("data")
        val data: T
)

class Reviews : ArrayList<Review>()

data class Review(
        @SerializedName("review_id")
        val id: Int,
        @SerializedName("rating")
        val rating: Float,
        @SerializedName("title")
        val title: String,
        @SerializedName("message")
        val message: String,
        @SerializedName("author")
        val author: String,
        @SerializedName("date")
        val date: String,
        @SerializedName("languageCode")
        val langCode: String,
        @SerializedName("traveler_type")
        val travelerType: String,
        @SerializedName("reviewerName")
        val reviewerName: String,
        @SerializedName("reviewerCountry")
        val reviewerCountry: String
)
