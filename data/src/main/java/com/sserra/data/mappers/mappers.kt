package com.sserra.data.mappers

import com.sserra.data.entities.Response
import com.sserra.data.entities.Reviews

/**
 *
 * Maps entities from data layer to domain layer.
 * In this case the representations in domain and data layers are identical, however in a more
 * complex application this may not be the case and converting data representation received from
 * server to an internal representation may be desired.
 */

private fun mapReview(res: com.sserra.data.entities.Review): com.sserra.domain.Review = com.sserra.domain.Review(
        title = res.title,
        id = res.id,
        rating = res.rating.toInt(),
        reviewerCountry = res.reviewerCountry,
        reviewerName = res.reviewerName,
        langCode = res.langCode,
        travelerType = res.travelerType,
        author = res.author,
        date = res.date,
        message = res.message
)

fun mapReviews(res: Response<Reviews>?): com.sserra.domain.Reviews {
    if (res?.data == null) {
        return com.sserra.domain.Reviews()
    }
    return com.sserra.domain.Reviews(res.data.map { mapReview(it) }, res.totalReviewsComments)
}
