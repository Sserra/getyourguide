package com.sserra.data.repos

import com.sserra.data.Api
import com.sserra.data.mappers.mapReviews
import com.sserra.domain.ReviewsCriteria
import com.sserra.domain.ReviewsRepository
import io.reactivex.Single


/**
 * Implementation of [ReviewsRepository] in data layer.
 * sergioserra99@gmail.com
 */
class ReviewsRepositoryImpl(private val api: Api.GetYourGuideService = Api.service) : ReviewsRepository {

    companion object {
        private const val TAG = "ReviewsRepositoryImpl"
    }

    override fun getReviews(criteria: ReviewsCriteria): Single<com.sserra.domain.Reviews> {
        return api.getReviews(
                criteria.city,
                criteria.tour,
                criteria.page,
                criteria.count,
                criteria.rating,
                "",
                criteria.sortBy.name.toLowerCase(),
                criteria.orderBy.name.toLowerCase()
        ).map { mapReviews(it) }
    }

    // In a more complex scenario this should encapsulate more details about the error
    // so the client knows what kind of error was it and what to do.
    class ReviewsRepoException(msg: String) : RuntimeException(msg)

}