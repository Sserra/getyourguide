package com.sserra.getyourguidereviews

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.SpinnerAdapter
import com.github.nitrico.lastadapter.LastAdapter
import com.sserra.domain.*
import com.sserra.getyourguidereviews.common.*
import com.sserra.getyourguidereviews.reviews.SortAdapter
import com.sserra.getyourguidereviews.reviews.StarsAdapter
import com.sserra.getyourguidereviews.vm.ReviewsViewModel
import com.sserra.getyourguidereviews.vm.ViewModelFactory
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter
import jp.wasabeef.recyclerview.adapters.AnimationAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.filter_bar_layout.*


class MainActivity : AppCompatActivity() {

    companion object {
        private const val city = "berlin-l17"
        private const val tour = "tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776"
    }

    private lateinit var vm: ReviewsViewModel
    private lateinit var mAdapter: AnimationAdapter
    private val reviews = mutableListOf<Review>()

    private var startsAdapter: SpinnerAdapter? = null
    private var sortAdapter: SpinnerAdapter? = null
    private var clear: Boolean = false

    // Current selected spinner positions
    private var sortPos = 0
    private var ratingPos = 0
    private var currentPage = 0

    private val stateObserver: StateObserver<Reviews> = object : StateObserver<Reviews>() {
        override fun onSuccess(data: Reviews?) {
            if (data != null) {

                errorBtn.hide()
                progress.hide()
                horizontalProgress.hide()
                list.show()

                if (filters.isInvisible) {
                    filters.enterReveal(cx = 0, cy = 0)
                }

                toolbar.subtitle = "${data.total} reviews"

                if (clear) {
                    clear = false
                    reviews.clear()
                    reviews.addAll(data.reviews)
                    mAdapter.notifyDataSetChanged()
                } else {
                    val count = mAdapter.itemCount
                    reviews.addAll(data.reviews)
                    mAdapter.notifyItemRangeChanged(count, mAdapter.itemCount)
                }

            }
        }

        override fun onError() {
            if (reviews.isEmpty()) {
                errorBtn.show()
            } else {
                showError()
            }
        }
    }

    private var criteria = ReviewsCriteria(city = city, tour = tour)

    override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (state != null) {
            sortPos = state(state, "sortPos")
            ratingPos = state(state, "ratingPos")
            currentPage = state(state, "currentPage")
        }

        // Very dummy error state, just show a button to retry.
        errorBtn.setOnClickListener { _ ->
            clear = true
            loadReviews()
        }

        initList()
        initSpinners()

        vm = ViewModelProviders.of(this, ViewModelFactory())[ReviewsViewModel::class.java]
        vm.getReviewsLiveData().observe(this, stateObserver)

        if (state == null) {
            loadReviews()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("sortPos", sortPos)
        outState.putInt("ratingPos", ratingPos)
        outState.putInt("currentPage", currentPage)
    }

    private fun loadReviews() {
        if (clear) {
            errorBtn.hide()
            list.hide()
            progress.show()
        }
        vm.getReviews(criteria)
    }

    private fun initList() {

        mAdapter = AlphaInAnimationAdapter(
                LastAdapter(reviews, BR.item)
                        .map(Review::class.java, R.layout.review_layout)
        )

        list?.apply {

            layoutManager = LinearLayoutManager(this@MainActivity)

            addItemDecoration(OffsetDecorator(8.px))

            addOnScrollListener(object : EndlessRecyclerViewScrollListener(10, layoutManager!!) {
                override fun onLoadMore(page: Int, totalItemsCount: Int) {
                    Log.d("MAIN", "Load more page: $page")
                    currentPage = page
                    clear = false
                    criteria = criteria.copy(page = page)
                    horizontalProgress.show()
                    loadReviews()
                }
            })

            adapter = mAdapter
        }

    }

    private fun initSpinners() {

        val sorts = resources.getStringArray(R.array.sorts).asList()
        val ratings = resources.getStringArray(R.array.stars).asList()

        startsAdapter = StarsAdapter(this, ratings)
        sortAdapter = SortAdapter(this, sorts)


        sortSpinner.adapter = sortAdapter
        sortSpinner.setSelection(sortPos)

        starsSpinner.adapter = startsAdapter
        starsSpinner.setSelection(ratingPos)

        sortSpinner.onItemClick {
            sortPos = it
            var sort: Sorts = Sorts.DATE_OF_REVIEW
            var order: Orders = Orders.DESC

            when (it) {
                0 -> {
                    sort = Sorts.DATE_OF_REVIEW
                    order = Orders.DESC
                }
                1 -> {
                    sort = Sorts.DATE_OF_REVIEW
                    order = Orders.ASC
                }
                2 -> {
                    sort = Sorts.RATING
                    order = Orders.DESC
                }
                3 -> {
                    sort = Sorts.RATING
                    order = Orders.ASC
                }
            }

            criteria = criteria.copy(sortBy = sort, orderBy = order, page = 0)
            clear = true
            loadReviews()
        }

        starsSpinner.onItemClick {
            ratingPos = it
            criteria = criteria.copy(rating = it, page = 0)
            clear = true
            loadReviews()
        }

    }

    private fun showError() {
        Snackbar.make(findViewById(android.R.id.content),
                getString(R.string.error),
                Snackbar.LENGTH_SHORT
        ).show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
