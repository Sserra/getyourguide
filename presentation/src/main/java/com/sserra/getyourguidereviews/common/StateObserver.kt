package com.sserra.getyourguidereviews.common

import android.arch.lifecycle.Observer


/**
 * State observers that calls the correct method depending when the state of a [Resource] object.
 * This easily improves code legibility and avoids error prone if's.
 *
 * @author Sérgio Serra on 25/09/2018.
 * sergioserra99@gmail.com
 */
abstract class StateObserver<T> : Observer<Resource<T>> {

    open fun onError() {}
    open fun onLoading() {}
    open fun onSuccess(data: T?) {}

    override fun onChanged(t: Resource<T>?) {
        t?.let {
            when {
                it.success -> onSuccess(it.data)
                it.error -> onError()
                else -> onLoading()
            }
        }
    }
}