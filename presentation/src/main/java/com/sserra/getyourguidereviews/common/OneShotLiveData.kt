package com.sserra.getyourguidereviews.common

import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.support.annotation.MainThread
import android.util.Pair
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicBoolean

class OneShotLiveData<T> : MutableLiveData<T>() {

    private val mPendingObservers = ConcurrentHashMap<Observer<T>, Pair<Observer<T>, AtomicBoolean>>()

    @MainThread
    override fun observe(owner: LifecycleOwner, observer: Observer<T>) {
        val interceptor = object : Observer<T> {
            override fun onChanged(t: T?) {

                var observerToTrigger: Observer<T>? = null

                synchronized(mPendingObservers) {
                    val pair = mPendingObservers[this]
                    if (pair != null && pair.second.compareAndSet(true, false)) {
                        observerToTrigger = mPendingObservers[this]?.first

                    }
                }

                if (observerToTrigger != null) {
                    observerToTrigger!!.onChanged(t)
                }
            }
        }

        synchronized(mPendingObservers) {
            mPendingObservers.put(interceptor, Pair.create<Observer<T>, AtomicBoolean>(observer, AtomicBoolean(false)))
        }

        // Observe the internal MutableLiveData
        super.observe(owner, interceptor)
    }

    override fun removeObserver(observer: Observer<T>) {
        super.removeObserver(observer)
        synchronized(mPendingObservers) {
            for ((key, value) in mPendingObservers) {
                if (observer === key || observer === value.first) {
                    mPendingObservers.remove(key)
                    break
                }
            }
        }
    }

    @MainThread
    override fun setValue(t: T?) {
        synchronized(mPendingObservers) {
            for (pending in mPendingObservers.values) {
                pending.second.set(true)
            }
        }
        super.setValue(t)
    }

    /**
     * Used for cases where T is Void, to make calls cleaner.
     */
    @MainThread
    fun call() {
        value = null
    }
}