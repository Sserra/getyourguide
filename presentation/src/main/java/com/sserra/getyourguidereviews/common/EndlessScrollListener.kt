package com.sserra.getyourguidereviews.common

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager

abstract class EndlessRecyclerViewScrollListener protected constructor(
        private val minItemsPerPage: Int = 0,
        layoutManager: RecyclerView.LayoutManager) : RecyclerView.OnScrollListener() {

    // The minimum amount of items to have below your current scroll position before loading more.
    private var visibleThreshold = 3
    // The current offset index of data you have loaded
    private var currentPage = 0
    // The total number of items in the dataset after the last load
    private var previousTotalItemCount = 0
    // True if we are still waiting for the last set of data to load.
    private var loading = true
    // Sets the starting page index
    private var startingPageIndex = 0

    private var mLayoutManager: RecyclerView.LayoutManager? = layoutManager

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        val lastVisibleItemPosition: Int = getLastVisibleItem(mLayoutManager)
        val totalItemCount = mLayoutManager!!.itemCount

        // If the total item count is zero and the previous isn't, assume the
        // list is invalidated and should be reset back to initial state
        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex
            this.previousTotalItemCount = totalItemCount
            if (totalItemCount == 0) {
                this.loading = true
            }
        }
        // If it’s still loading, we check to see if the dataset count has
        // changed, if so we conclude it has finished loading and update the current page
        // number and total item count.
        if (loading && totalItemCount > previousTotalItemCount) {
            loading = false
            previousTotalItemCount = totalItemCount
        }

        // If it isn’t currently loading, we check to see if we have breached
        // the visibleThreshold and need to reload more data.
        // If we do need to reload some more data, we notifyViewWithStartInitializer onLoadMore to fetch the data.
        // threshold should reflect how many total columns there are too
        if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount && totalItemCount >= minItemsPerPage) {
            currentPage++
            onLoadMore(currentPage, totalItemCount)
            loading = true
        }

    }

    // Defines the process for actually loading more data based on page
    abstract fun onLoadMore(page: Int, totalItemsCount: Int)

    fun reset() {
        visibleThreshold = 3
        currentPage = 0
        previousTotalItemCount = 0
        loading = true
        startingPageIndex = 0
    }

}

fun getLastVisibleItem(layoutManager: RecyclerView.LayoutManager?): Int {
    if (layoutManager == null) {
        return 0
    }

    return when (layoutManager) {
        is StaggeredGridLayoutManager -> {
            val lastVisibleItemPositions = layoutManager.findLastVisibleItemPositions(null)
            // Get maximum element within the list.
            getLastVisibleItemArray(lastVisibleItemPositions)
        }
        is GridLayoutManager -> layoutManager.findLastVisibleItemPosition()
        is LinearLayoutManager -> layoutManager.findLastVisibleItemPosition()
        else -> 0
    }

}

private fun getLastVisibleItemArray(lastVisibleItemPositions: IntArray): Int {
    var maxSize = 0
    for (i in lastVisibleItemPositions.indices) {
        if (i == 0) {
            maxSize = lastVisibleItemPositions[i]
        } else if (lastVisibleItemPositions[i] > maxSize) {
            maxSize = lastVisibleItemPositions[i]
        }
    }
    return maxSize
}
