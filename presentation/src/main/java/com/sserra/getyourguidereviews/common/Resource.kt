package com.sserra.getyourguidereviews.common

import com.sserra.getyourguidereviews.common.Resource.Status.*

/**
 * @author Sérgio Serra on 24/09/2018.
 * sergioserra99@gmail.com
 */
data class Resource<T> constructor(private val status: Status, val data: T?, val message: String?) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }

    val success: Boolean
        get() = status == SUCCESS

    val error: Boolean
        get() = status == ERROR

    val isLoading: Boolean
        get() = status == LOADING

    companion object {
        fun <T> success(data: T?): Resource<T> = Resource(SUCCESS, data, null)
        fun <T> error(msg: String): Resource<T> = Resource(ERROR, null, msg)
        fun <T> loading(): Resource<T> = Resource(LOADING, null, null)
    }
}