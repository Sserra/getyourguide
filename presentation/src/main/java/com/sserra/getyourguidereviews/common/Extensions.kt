package com.sserra.getyourguidereviews.common

import android.app.Activity
import android.content.res.Resources
import android.os.Build
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.view.View
import android.view.ViewAnimationUtils
import android.widget.AdapterView
import android.widget.Spinner


/**
 * Utility kotlin extensions to reduce boilerplate and improve code legibility.
 */
val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

@Suppress("UNCHECKED_CAST")
fun <T> Activity.state(state: Bundle, key: String): T = state.get(key) as T


fun View.show(): View {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
    return this
}

fun View.hide(): View {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
    return this
}

fun Spinner.onItemClick(listener: (pos: Int) -> Unit) {
    onItemSelectedListener = SelectedListener(listener = listener)
}

class SelectedListener(val listener: (pos: Int) -> Unit) : AdapterView.OnItemSelectedListener {

    private var pos: Int = 0
    override fun onNothingSelected(p0: AdapterView<*>?) {}

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (pos != p2) {
            pos = p2
            listener(p2)
        }
    }
}

val View.isInvisible: Boolean
    get() = visibility == View.INVISIBLE

fun View.enterReveal(cx: Int = measuredWidth / 2, cy: Int = measuredHeight / 2) {

    // get the final radius for the clipping circle
    val finalRadius = Math.max(width, height) / 2

    // create the animator for this view (the start radius is zero)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, 0f, finalRadius.toFloat())
        anim.start()
    } else {
        ViewCompat.animate(this)
                .alpha(1F)
                .duration = 250
    }
    visibility = View.VISIBLE
}