package com.sserra.getyourguidereviews.reviews

import android.content.Context
import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.sserra.getyourguidereviews.R
import com.sserra.getyourguidereviews.common.px


class StarsAdapter(context: Context, resource: Int, objects: List<String>) : ArrayAdapter<String>(context, resource, objects) {

    private lateinit var mInflater: LayoutInflater
    private var mStarDrawable: Drawable? = null

    constructor(context: Context, objects: List<String>) : this(context, android.R.layout.simple_dropdown_item_1line, objects) {
        mInflater = LayoutInflater.from(context)
        mStarDrawable = ContextCompat.getDrawable(context, R.drawable.ic_star)
        mStarDrawable?.let {
            DrawableCompat.setTint(it, ContextCompat.getColor(context, R.color.starColor))
            val size = 17.px
            it.setBounds(0, 0, size, size)
        }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var v: View? = convertView
        if (v == null) {
            v = mInflater.inflate(R.layout.stars_spinner_view, parent, false)
        }

        v?.let {
            it.findViewById<TextView>(R.id.stars).apply {
                text = getItem(position)
                setCompoundDrawables(null, null, mStarDrawable, null)
            }
        }

        return v!!
    }

}


class SortAdapter(context: Context, resource: Int, objects: List<String>) : ArrayAdapter<String>(context, resource, objects) {

    private lateinit var mInflater: LayoutInflater

    constructor(context: Context, objects: List<String>) : this(context, android.R.layout.simple_dropdown_item_1line, objects) {
        mInflater = LayoutInflater.from(context)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        var v: View? = convertView
        if (v == null) {
            v = mInflater.inflate(R.layout.sort_spinner_view, parent, false)
        }

        v?.findViewById<TextView>(R.id.subtitle)?.text = getItem(position)

        return v!!
    }

}