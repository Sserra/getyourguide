package com.sserra.getyourguidereviews.vm

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.sserra.data.repos.ReviewsRepositoryImpl


/**
 * @author Sérgio Serra on 24/09/2018.
 * Criations
 * sergioserra99@gmail.com
 */
class ViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {

        if (modelClass.isAssignableFrom(ReviewsViewModel::class.java)) {
            return ReviewsViewModel(ReviewsRepositoryImpl()) as T
        }

        return modelClass.newInstance()
    }
}