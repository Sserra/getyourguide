package com.sserra.getyourguidereviews.vm

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.sserra.domain.Review
import com.sserra.domain.Reviews
import com.sserra.domain.ReviewsCriteria
import com.sserra.domain.ReviewsRepository
import com.sserra.getyourguidereviews.common.Resource
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * sergioserra99@gmail.com
 */
class ReviewsViewModel(private val repo: ReviewsRepository) : ViewModel() {

    private val reviewsLv: MutableLiveData<Resource<Reviews>> = MutableLiveData()

    fun getReviewsLiveData() = reviewsLv

    fun getReviews(criteria: ReviewsCriteria) {
        val d = repo
                .getReviews(criteria)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { reviews, error ->
                    if (error == null) {
                        reviewsLv.value = Resource.success(reviews)
                        return@subscribe
                    }
                    error.printStackTrace()
                    reviewsLv.value = Resource.error(error.localizedMessage)
                }
    }

}