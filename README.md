GetYourGuide
====================

This repo contains code for GetYourGuide code challenge.

Project structure:

* Modules
    * **Presentation module**, UI logic, activities, views...
    * **Domain layer**, very simple module that contains repository contract and domain entities
    * **Data layer**, repository implementation, Api, data sources, mappers, data entities

* Tests
    * Data module contains an unit test example for repository