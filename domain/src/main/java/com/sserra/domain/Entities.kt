package com.sserra.domain

import java.io.Serializable

/**
 * Entities representation in domain layer. This is a simple use case so the entities are equal
 * to
 */
data class Reviews(val reviews: List<Review> = listOf(), val total: Int = 0)

data class Review(
        val id: Int,
        val rating: Int,
        val title: String? = "",
        val message: String? = "",
        val author: String? = "",
        val date: String? = "",
        val langCode: String? = "",
        val travelerType: String? = "",
        val reviewerName: String? = "",
        val reviewerCountry: String? = ""
) : Serializable