package com.sserra.domain

import io.reactivex.Single


/**
 * Repository contract for data layer to implement.
 *
 * sergioserra99@gmail.com
 */
interface ReviewsRepository {
    fun getReviews(criteria: ReviewsCriteria): Single<Reviews>
}


enum class Sorts {
    DATE_OF_REVIEW, RATING
}

enum class Orders {
    ASC, DESC
}

/**
 * Criteria object used in [ReviewsRepository] to parameterize the request.
 */
data class ReviewsCriteria(
        val city: String,
        val tour: String,
        val page: Int = 0,
        val count: Int = 10,
        val rating: Int = 0,
        val sortBy: Sorts = Sorts.DATE_OF_REVIEW,
        val orderBy: Orders = Orders.DESC
)